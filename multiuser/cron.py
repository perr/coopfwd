#!/usr/bin/python
import imaplib
import smtplib
import sys
import sqlite3

from config import *
from crypto import encrypt, decrypt

conn = sqlite3.connect(INSTALL_DIR+"coopfwd.db")
c = conn.cursor()
c.execute("select * from users")
rows = c.fetchall()

for row in rows:
    USERNAME = row[0]
    PASSWORD = decrypt(row[1])
    FORWARDTO = row[2]
    DATE = row[3]


    S = False
    M = imaplib.IMAP4_SSL("farley2.cooper.edu", 993)
    M.login(USERNAME, PASSWORD)
    M.select("INBOX")
    typ, data = M.search(None, '(SINCE "%s" UNSEEN)'%(DATE))
    print data
    if len(data[0].split()):
    	S = smtplib.SMTP_SSL('farley2.cooper.edu', 465)
#	S.connect()
#        print username, password
	S.login(USERNAME, PASSWORD)
	for num in data[0].split():
		print "forwarding email"
		typ, data = M.fetch(num, '(RFC822)')
		S.sendmail("forward@cooper.edu", FORWARDTO, data[0][1])
    M.close()
    M.logout()
    if S:
		S.quit()


