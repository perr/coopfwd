import web
from config import SSL_KEY, SSL_CERT

from web.wsgiserver import CherryPyWSGIServer

CherryPyWSGIServer.ssl_certificate = SSL_CERT
CherryPyWSGIServer.ssl_private_key = SSL_KEY



urls = (
    '/.*', 'index'
)

https=True

app = web.application(urls, globals())

render = web.template.render('templates')

class index:
    def GET(self):
        https=True
        return render.index("")

    def POST(self):
        from coopfwdmanage import process
        i = web.input()
        return render.index(process(i["uname"], i["passwd"], i["fwd"]))

if __name__ == "__main__":
    app.run()
