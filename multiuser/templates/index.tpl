$def with (response)

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cooper.edu E-Mail Forwarding</title>

<script type="text/javascript">
function showPoptart(){
	 document.getElementById("poptart").style.visibility = "visible";
	 return true;
}
</script>

</head>
<body>
<span style="color:green;"><b>
$response
</b></span>
<h1>cooper.edu email forwarding</h1>

<hr/>

<form id="normal" class="general" action="." method="post" onsubmit="showPoptart()">
<table border=0>
   <tr>
      <td><b>username:</b></td>
      <td><input name="uname" type="text"></input><span style="color:gray;">@cooper.edu</span></td>
   </tr><tr>
      <td><b>password:</b></td>
      <td><input name="passwd" type="password"></input></td>
   </tr><tr>
      <td><b>forward to:</b></td>
      <td><input name="fwd" type="text"></input><span style="color:gray;">(leave blank to disable forwarding)</span></td>
   </tr>
</table>
      <br/>
<input style="width:150px;margin-left:80px;" title="Submit" alt="Submit" name="submitButton" id="submitButton" type="submit" value="Submit" class="submit" />&nbsp;&nbsp;&nbsp;&nbsp;<img id="poptart" src="static/poptart.gif" style="visibility:hidden;"></img> 
</form>


<hr/>

<h3>Is this secure?</h3>
Pretty much. Your password is encrypted every time it is transmitted or stored. Still, anybody with root or physical access to this server can theoretically obtain your password. Use at your own risk.

<h3>Is this reliable?</h3>
Yes. If the server goes down, emails will be forwarded as soon as the server is restarted. Emails will not get "lost" and you can always use webmail.cooper.edu. Emails that have been forwarded will be "marked as read," emails that have not yet been forwarded will appear "new." Nothing on Cooper's servers are deleted by this service.

<h3>Is there any delay?</h3>
New email messages may take up to 5 minutes to forward.

<h3>Is the source code available?</h3>
<a href="https://bitbucket.org/perr/coopfwd/src">Of course.</a>

<h3>Is this official?</h3>
No

<h3>Do I love you?</h3>
<3 Yes <3

<br/>
<br/>
<br/>
<br/>
<div style="padding-top:1em;position:fixed;border-top:1px solid;bottom:0px;left:0px;width:100%;background-color:white;height:2em;">
<a style="color:gray;padding-left:40px;" href="http://jperr.com">jordan perr - 2011</a>
</div>
</body>
</html>
      
<?php } ?>
