#I borrow some code from
#http://www.codekoala.com/blog/2009/aes-encryption-python-using-pycrypto/

from Crypto.Cipher import AES
from Crypto.Hash import SHA256
import base64
import os

from config import secret

BLOCK_SIZE = 32
PADDING = '{'
pad = lambda s: s + "|" + (BLOCK_SIZE - (len(s)+1) % BLOCK_SIZE) * PADDING
EncodeAES = lambda c, s: c.encrypt(pad(s))
DecodeAES = lambda c, e: c.decrypt(e).rstrip(PADDING)[:-1]

def encrypt(plaintext):
    cipher1 = AES.new(secret)
    ctext1 = EncodeAES(cipher1, plaintext)
    #ctext 1 is a base64 encoded string at least 32 bytes long
    hasher1 = SHA256.new(ctext1)
    hash1 = hasher1.hexdigest()[:32]
    cipherhash = EncodeAES(cipher1, hash1)
    cipher2 = AES.new(cipherhash[:32])
    ctext2 = EncodeAES(cipher2, plaintext)
    return hash1+base64.b64encode(ctext2)

def decrypt(ciphertext):
    hash1 = ciphertext[:32]
    ctext2 = base64.b64decode(ciphertext[32:])
    cipher1 = AES.new(secret)
    cipherhash = EncodeAES(cipher1, hash1)
    cipher2 = AES.new(cipherhash[:32])
    plaintext = DecodeAES(cipher2, ctext2)
    return plaintext
    
if __name__=="__main__":
    print decrypt(encrypt("test")) == "test"
