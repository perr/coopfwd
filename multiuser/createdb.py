import sqlite3
from config import *

conn = sqlite3.connect(INSTALL_DIR+'coopfwd.db')

c = conn.cursor()

c.execute('''create table users (username text, password text, fwd text, date text)''')

conn.commit()

c.close()