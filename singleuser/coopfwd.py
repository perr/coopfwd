#!/usr/bin/python
"""
COOPFWD SINGLE USER SCRIPT
Author: Jordan Perr-Sauer
"""


# Edit the four options below

USERNAME  = "username"#@cooper.edu

PASSWORD  = "password"

FORWARDTO = "forward@email.com"

DATE      = "25-Jan-2012" #emails arrived before this date are ignored. Set it to TODAY'S DATE or you'll get a bunch of old, unread emails forwarded on first run.

# Do not edit past here



import imaplib
import smtplib
import sys

S = False
M = imaplib.IMAP4_SSL("farley2.cooper.edu", 993)
M.login(USERNAME, PASSWORD)
M.select("INBOX")
typ, data = M.search(None, '(SINCE "%s" UNSEEN)'%(DATE))
print data
if len(data[0].split()):
    S = smtplib.SMTP_SSL('farley2.cooper.edu', 465)
    S.login(USERNAME, PASSWORD)
    for num in data[0].split():
        print "forwarding email"
        typ, data = M.fetch(num, '(RFC822)')
        S.sendmail("forward@cooper.edu", FORWARDTO, data[0][1])
M.close()
M.logout()
if S:
    S.quit()
